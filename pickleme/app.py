import pickle
import base64
from flask import Flask, request, render_template, redirect

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/cucumber')
def cu_cumber():
    try:
        data = base64.urlsafe_b64decode(request.values.get('pickled'))
        deserialized = pickle.loads(data)
    except:
        return redirect('https://thekitchencommunity.org/cucumbers-vs-pickles/')

@app.route('/pickel', methods=['POST'])
def pickel_me():
    try:
        data = base64.urlsafe_b64decode(request.form['pickled'])
        deserialized = pickle.loads(data)
        return '', 204
    except:
        return 'cucumber?', 418

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
